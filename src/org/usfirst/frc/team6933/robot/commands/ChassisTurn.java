package org.usfirst.frc.team6933.robot.commands;

import org.usfirst.frc.team6933.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 * This command drives at a set speed until it is stopped
 * 
 * @author Benji
 *
 */
public class ChassisTurn extends Command
{
	private double motorSpeed;
	
	public ChassisTurn(double motorSpeed)
	{
		this.requires(Robot.chassis);
		this.motorSpeed = motorSpeed;
	}
	
	@Override
	protected void initialize() {
		
	}

	@Override
	protected void execute() {
		Robot.chassis.drive(0, motorSpeed);
	}

	@Override
	protected boolean isFinished() {
		return false;
	}

	@Override
	protected void end() {
		Robot.chassis.drive(0, .1);
	}

	@Override
	protected void interrupted() {
	}
}
