package org.usfirst.frc.team6933.robot.subsystems;

import org.usfirst.frc.team6933.robot.Robot;
import org.usfirst.frc.team6933.robot.RobotMap;
import org.usfirst.frc.team6933.robot.commands.ChassisTeleop;

import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;


/**
 *
 */
public class Chassis extends Subsystem {
	// Put methods for controlling this subsystem
	// here. Call these from Commands.
	
	
	
	// Define the two motors as CANTalons
	public static CANTalon leftMotorA = new CANTalon(RobotMap.CAN.motorLeftA);
	public static CANTalon leftMotorB = new CANTalon(RobotMap.CAN.motorLeftB);
	public static CANTalon rightMotorA = new CANTalon(RobotMap.CAN.motorRightA);
	public static CANTalon rightMotorB = new CANTalon(RobotMap.CAN.motorRightB);

	RobotDrive drive = new RobotDrive(leftMotorA, leftMotorB, rightMotorA, rightMotorB);
	
	public void initDefaultCommand() {
		setDefaultCommand(new ChassisTeleop());
	}
	
	public void drive(double forwardAxis, double turnAxis) 
	{
		drive.arcadeDrive(forwardAxis, turnAxis);
	}
	

}
