package org.usfirst.frc.team6933.robot.commandgroups;

import org.usfirst.frc.team6933.robot.commands.ChassisDrive;
import org.usfirst.frc.team6933.robot.commands.ChassisTurn;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class Autonomous1 extends CommandGroup 
{
	public Autonomous1()
	{
		this.addSequential(new ChassisDrive(.3), 3);
		this.addSequential(new ChassisTurn(.5), 3);
	}
}
